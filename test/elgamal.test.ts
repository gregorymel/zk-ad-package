import { TestingAppChain } from "@proto-kit/sdk";
import { PrivateKey, UInt64, Group, verify, Scalar } from "o1js";
import { Balances } from "../src/Balances";
import { decryption, DecryptionPublicInput, ElGamalECC } from "../src/elgamal";
import { log } from "@proto-kit/common";

log.setLevel("ERROR");

describe("balances", () => {
  it("should encrypt/decrypt msgs", async () => {
    const { verificationKey } = await decryption.compile();

    const sk = PrivateKey.random();
    const pk = sk.toPublicKey();

    const plaintext = Scalar.from(BigInt(10));
    const msg = ElGamalECC.encryptMsg(BigInt(10));
    const cipher = ElGamalECC.encrypt(msg, pk);

    // const recivedMsg = ElGamalECC.decrypt(cipher, sk);

    const subMsg = ElGamalECC.encryptMsg(BigInt(5));
    const subCipher = ElGamalECC.encrypt(subMsg, pk);
    const decryptionInput = new DecryptionPublicInput({
      cipher: cipher,
      subCipher: subCipher,
      value: Scalar.from(5)
    });

    // console.log(recivedMsg);

    const secretMsg = ElGamalECC.getSecret(cipher, sk);
    const secretVal = ElGamalECC.getSecret(subCipher, sk);

    const proof = await decryption.baseProof(decryptionInput, plaintext, secretMsg, secretVal);
    const res = await verify(proof.toJSON(), verificationKey);
    
    expect(res).toBe(true);
  }, 1_000_000);
});