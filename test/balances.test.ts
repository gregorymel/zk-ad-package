import { TestingAppChain } from "@proto-kit/sdk";
import { PrivateKey, UInt64 } from "o1js";
import { Balances } from "../src/Balances";
import { log } from "@proto-kit/common";
import { ElGamalECC } from "../src/elgamal";
import { BalancesInputs } from "../src/Balances";

log.setLevel("ERROR");

function getInputData(value: bigint) {
    const privateKey = PrivateKey.random();
    const publicKey = privateKey.toPublicKey();

    const msg = ElGamalECC.encryptMsg(value);
    const cipher = ElGamalECC.encrypt(msg, publicKey);

    return {
        publicKey,
        privateKey,
        cipher
    }
}

describe("balances", () => {
    it("should mint encrypted balances", async () => {
        const appChain = TestingAppChain.fromRuntime({
            modules: {
            Balances,
            },
            config: {
            Balances: {},
            },
        });

        await appChain.start();

        const alicePrivateKey = PrivateKey.random();
        const alice = alicePrivateKey.toPublicKey();

        appChain.setSigner(alicePrivateKey);

        const balances = appChain.runtime.resolve("Balances");


        const value = BigInt(10);
        const user1 = getInputData(value);
        const user2 = getInputData(value);

        const input = new BalancesInputs({
            keys: [user1.publicKey, user2.publicKey],
            ciphers: [user1.cipher, user2.cipher]
        });

        const tx1 = await appChain.transaction(alice, () => {
            balances.addBalances(input);
        });

        await tx1.sign();
        await tx1.send();

        const block = await appChain.produceBlock();

        const balance = await appChain.query.runtime.Balances.balances.get(user1.publicKey);
        const res = balance?.c.equals(user1.cipher.c);
        expect(res?.toBoolean()).toBe(true);

        expect(block?.txs[0].status).toBe(true);
    //     expect(balance?.toBigInt()).toBe(1000n);
    }, 1_000_000);
});