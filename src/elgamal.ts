import {
    Group,
    PublicKey,
    Scalar,
    PrivateKey,
    Experimental,
    Struct,
    Provable,
    Int64,
    UInt32,
    Field
} from 'o1js';
import { SIZE } from "./constants";

export class Cipher extends Struct({
    c: Group,
    d: Group,
}) {
    add(rhs: Cipher): Cipher {
        return new Cipher({ c: this.c.add(rhs.c), d: this.d.add(rhs.d)});
    }

    sub(rhs: Cipher): Cipher {
        return new Cipher({ c: this.c.sub(rhs.c), d: this.d.sub(rhs.d)});
    }
}

export class ElGamalECC {
    private static G = Group.generator;

    static encryptMsg(msg: bigint): Group {
        return this.G.scale(Scalar.from(msg));
    }
  
    static encrypt(msg: Group, y: PublicKey): Cipher {
      let k = Experimental.memoizeWitness(Scalar, Scalar.random);
      return new Cipher({
        c: this.G.scale(k),
        d: y.toGroup().scale(k).add(msg),
      });
    }
  
    static decrypt(cipher: Cipher, priv: PrivateKey): Group {
      let x = priv.s;
      let c_ = cipher.c.scale(x);
      let pm = cipher.d.sub(c_);
      return pm;
    }

    static getSecret(cipher: Cipher, priv: PrivateKey): Group {
        let x = priv.s;
        return cipher.c.scale(x);
    }
}

export class EncryptionPublicInput extends Struct({
    ciphers: Provable.Array(Cipher, SIZE)
}) {
    getCipher(i: Int64): Cipher {
        let element = new Cipher({ c: Group.generator, d: Group.generator });
        for (let j = 0; j < this.ciphers.length; j++) {
            element = Provable.if<Cipher>(Int64.from(j).equals(i), Cipher, this.ciphers[j], element);
        }
        return element;
    }
}

export const encryption = Experimental.ZkProgram({
    publicInput: EncryptionPublicInput,
    methods: {
        baseProof: {
            privateInputs: [],
            method: () => {
                // ElGamalFF.encrypt();
            }
        },
    },
});

export class EncryptionProof extends Experimental.ZkProgram.Proof(encryption) {}

export class DecryptionPublicInput extends Struct({
    cipher: Cipher,
    subCipher: Cipher,
    value: Scalar
}) {}

export const decryption = Experimental.ZkProgram({
    publicInput: DecryptionPublicInput,
    methods: {
        baseProof: {
            privateInputs: [Scalar, Group, Group],
            method: (publicInput: DecryptionPublicInput, plaintext: Scalar, secretMsg: Group, secretVal: Group) => {
                const msg = Group.generator.scale(plaintext);
                publicInput.cipher.d.sub(secretMsg).assertEquals(msg);

                const val = Group.generator.scale(publicInput.value);
                publicInput.subCipher.d.sub(secretVal).assertEquals(val);

                // UInt32.fromFields(Scalar.toFields(publicInput.value))
                //     .lessThan(
                //         UInt32.fromFields(Scalar.toFields(plaintext))
                //     )
                //     .assertTrue();
            }
        },
    },
});
  
export class DecryptionProof extends Experimental.ZkProgram.Proof(decryption) {}