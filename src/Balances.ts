import "reflect-metadata";
import {
  RuntimeModule,
  runtimeModule,
  state,
  runtimeMethod,
} from "@proto-kit/module";
import { State, StateMap, assert } from "@proto-kit/protocol";
import { PublicKey, Provable, Struct, Int64, Group } from "o1js";
import { DecryptionProof, Cipher } from './elgamal';
import { SIZE } from "./constants";

export class BalancesInputs extends Struct({
  keys: Provable.Array(PublicKey, SIZE),
  ciphers: Provable.Array(Cipher, SIZE),
}) {
  getCipher(i: Int64): Cipher {
    let element = new Cipher({ c: Group.generator, d: Group.generator });
    for (let j = 0; j < this.ciphers.length; j++) {
        element = Provable.if<Cipher>(Int64.from(j).equals(i), Cipher, this.ciphers[j], element);
    }
    return element;
  }

  getKey(i: Int64): PublicKey {
    let element = PublicKey.empty();
    for (let j = 0; j < this.ciphers.length; j++) {
        element = Provable.if<PublicKey>(Int64.from(j).equals(i), PublicKey, this.keys[j], element);
    }
    return element;
  }
};

@runtimeModule()
export class Balances extends RuntimeModule<any> {
  @state() public balances = StateMap.from<PublicKey, Cipher>(
    PublicKey,
    Cipher
  );

  @runtimeMethod()
  public addBalances(inputs: BalancesInputs): void {
    for (let i = 0; i < inputs.keys.length; i++) {
      const address = inputs.getKey(Int64.from(i));
      const amount = inputs.getCipher(Int64.from(i));
      const currentBalance = this.balances.get(address);
      const newBalance = currentBalance.value.add(amount);
      this.balances.set(address, newBalance);
    }
  }

  @runtimeMethod()
  public withdraw(address: PublicKey, proof: DecryptionProof): void {
    proof.verify();
    const currentBalance = this.balances.get(address).value;
    assert(
      proof.publicInput.cipher.c.equals(
        currentBalance.c
      ),
      "Invalid cyphertext"
    );
    const newBalance = currentBalance.sub(proof.publicInput.subCipher);
    this.balances.set(address, newBalance);
  }
}