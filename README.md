## About zk-ad-package

It's a PoC system that controls an input cashflow while concealing the transfer amounts of specific users.

## Idea

The main idea is the application of Elgamal additive homomorphic encryption to hide influencers' balances and their changes. We store all influencers' balances encrypted, and all operations, such as *transfer*, are performed on ciphertexts. When making a withdrawal, an influencer only needs to disclose the amount of tokens they wish to take from the protocol. They do not need to disclose their intermediate prices and actions. We utilized Mina's zk programs to create verifiable decryption without revealing prices and private keys of participants. Additionally, we employed the protokit framework to streamline the process of building apps on Mina.


## Elgamal on ECC - Encryption, Decpryption

```bash
src/elgamal.ts
```

Here you can find implementation of Elgamal encryption scheme on ECC, ZkPrograms for building verifiable decryption and more.


## Encrypted Balances

```bash
src/Balances.ts
```

Here you can find a PoC implementation of a state with encrypted users' balances and operations on these balances.